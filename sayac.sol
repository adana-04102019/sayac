pragma solidity >=0.4.22 <0.6.0;

contract Sayac {
    int sayac;
    
    constructor() public {
        sayac= 0;
        
    }
    
    function getSayac() view public returns(int) {
        return sayac;
    } 
    
    function increment() public {
        sayac = sayac + 1;
    }
}